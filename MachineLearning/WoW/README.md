WoW Gold Making through Auctions ML-DL bot
The idea is that by using machine Machine Learning algorithms, "Big Data"(1) from the in-game auction house (or reliable information gathered it) and an initial gold budget the bot would predict changes in item prices and would buy and sell items accordingly to cap the "Gold Limit"(3) on the active character as fast as possible, without getting my account blocked or banned.
This objective is interesting to me because unlike most machine learning algorithms, the bot will have to use its main drive value, which is currency, to gain more currency. I do have a hypothesis though, that an additional drive value will have to be made.
Another objective would be that hopefully I can use what I learned while making this bot to make gold with cryptocurrencies.

Other ideas that can be used to improve the algorithm:
I have a gut feeling that the active people in the Auction House room influence variables from "Big Data"(1).
The Algorithm could probably find some efficiency in the "Auction Life"(2)
"Deposit Price"(4).
Bids vs. Buyouts?

Limitations:
"Initial budget"(0)
"Auction Life"(2)

Losing/Winning:
The bot is deemed a failure, if it goes 50% under initial budget.
The bot is considered "Done" when it caps the gold limit.

Milestones:
100% average prediction (counts only after 5 sells)
100% of initial budget gold made.
200k gold made.
1 million gold made.
5 million gold made.

"Initial budget"(0): User defined variable. 1000g for my first generation. More if the bots succeeds or reaches a milestone.

"Big Data"(1):
Price,
Supply (optimal undercuts, chances of the item being sold),
Demand (chances of the item being sold),
Time (when the item was sold),
Relationships (X can influence the price change in Y),

"Auction Life"(2): 
An Auction can only live for 48 hours Max. But there's an option of making available for 12, 24 hours too.

"Gold Limit"(3):
9,999,999 gold coins

"Deposit Price"(4):
It costs gold to post an auction.


