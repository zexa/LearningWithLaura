#!/usr/bin/env python

last_fizzbuzz = 0
last_fizz = 0
last_buzz = 0

while (True):
    x = input("fizzbuzz/fizz/buzz/done: ")
    if (x.lower() == "fizzbuzz"):
        while (True):
            last_fizzbuzz+=1
            if last_fizzbuzz%3==0 and last_fizzbuzz%5==0:
                print("Have a FizzBuzz:", last_fizzbuzz)
                break
    elif (x.lower() == "fizz"):
        while (True):
            last_fizz+=1
            if last_fizz%3==0:
                print("Have a Fizz:", last_fizz)
                break
    elif (x.lower() == "buzz"):
        while (True):
            last_buzz+=1
            if last_buzz%5==0:
                print("Have a Buzz:", last_buzz)
                break
    elif (x.lower() == "done"):
        break
    elif (x.isdigit()):
        if int(x)%3==0 and int(x)%5==0:
            print("FizzBuzz")
        elif int(x)%3==0:
            print("Fizz")
        elif int(x)%5==0:
            print("Buzz")
        else:
            print(x)
    else:
        print("I don't understand :(")
